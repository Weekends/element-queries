/*
  TODO
  - [ ] How to deal with circularity? E.g., parent element changing width with 
        ltriggers a new width change on the child? 
  - [ ] Playing nice with media queries

*/

import debounce from './debounce.js'


module.exports = () => {
  // TODO, graceful fail or polyfill
  // typeof document.querySelectorAll == "undefined"
  // if ( !( 'querySelector' in w.document ) || !( 'getComputedStyle' in w ) )
  //   return

  var selector = '[eq]'
  var els
  
  
  function selectEls(){
    /* In Chrome on Mac the order of elements in querySelectorAll is top-down,
    e.g., body first then the first div in that then all that first div's 
    descendants, then the second div in the body followed by all its 
    descendants. 
      If this is consistent in all browsers then circularity is prevented by 
    first applying parent element styles (which may mean a jump to 2x column, 
    which effects children widths in turn) followed by child widths. 
      Because we're measuring element widths, it may be necessary to manually 
    trigger re-paints for children to detect correct styles.
      For this reason, adding new elements should re-query the DOM to get a new 
    tree ordered element list.
    */
    
    els = Array.prototype.slice.call(document.querySelectorAll(selector))
    // ^ Convert node list to array
    doEls()
  }
  
  
  function doEls(){
    if (els && els.length)
      els.forEach( applyBreakpointAttributes )
  }
  
  
  
  function applyBreakpointAttributes(el){
    console.log( el.clientWidth )
    // return false
    
    let width = el.offsetWidth, // clientWidth,
        min_widths = [],
        max_widths = []
    
    
    // getComputedStyle( el, ":before" ).getPropertyValue( "content" ) // ???
    // let breakpoints = ( getComputedStyle( el, ':before' ).content )
    let breakpoints = ( getComputedStyle(el).content )
      .split('"').join('') // Firefox returns quotes, e.g., '"Content string"'
      .trim()
      .split(' ')
      .forEach(breakpoint => {
        // console.log( breakpoint )
        breakpoint = parseInt(breakpoint, 10)
        if ( width >= breakpoint )
          min_widths.push(breakpoint)
        else 
          max_widths.push(breakpoint)
      })
    
    
    el.setAttribute('min-width', min_widths.join(' '))
    el.setAttribute('max-width', max_widths.join(' '))
  }
  

  // Setup
  selectEls()
  
  window.onresize = debounce(doEls, 100)
  
  
  return {
    update: selectEls,
  }
}
