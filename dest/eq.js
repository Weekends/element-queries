(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);throw new Error("Cannot find module '"+o+"'")}var f=n[o]={exports:{}};t[o][0].call(f.exports,function(e){var n=t[o][1][e];return s(n?n:e)},f,f.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
"use strict";

var debounce = function debounce(func, wait, immediate) {
  var timeout;
  return function () {
    var context = this,
        args = arguments;
    clearTimeout(timeout);
    timeout = setTimeout(function () {
      timeout = null;
      if (!immediate) func.apply(context, args);
    }, wait);
    if (immediate && !timeout) func.apply(context, args);
  };
};

module.exports = debounce;

},{}],2:[function(require,module,exports){
/*
  TODO
  - [ ] How to deal with circularity? E.g., parent element changing width with 
        ltriggers a new width change on the child? 
  - [ ] Playing nice with media queries

*/

'use strict';

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

var _debounceJs = require('./debounce.js');

var _debounceJs2 = _interopRequireDefault(_debounceJs);

module.exports = function () {
  // TODO, graceful fail or polyfill
  // typeof document.querySelectorAll == "undefined"
  // if ( !( 'querySelector' in w.document ) || !( 'getComputedStyle' in w ) )
  //   return

  var selector = '[eq]';
  var els;

  function selectEls() {
    /* In Chrome on Mac the order of elements in querySelectorAll is top-down,
    e.g., body first then the first div in that then all that first div's 
    descendants, then the second div in the body followed by all its 
    descendants. 
      If this is consistent in all browsers then circularity is prevented by 
    first applying parent element styles (which may mean a jump to 2x column, 
    which effects children widths in turn) followed by child widths. 
      Because we're measuring element widths, it may be necessary to manually 
    trigger re-paints for children to detect correct styles.
      For this reason, adding new elements should re-query the DOM to get a new 
    tree ordered element list.
    */

    els = Array.prototype.slice.call(document.querySelectorAll(selector));
    // ^ Convert node list to array
    doEls();
  }

  function doEls() {
    if (els && els.length) els.forEach(applyBreakpointAttributes);
  }

  function applyBreakpointAttributes(el) {
    console.log(el.clientWidth);
    // return false

    var width = el.offsetWidth,
        // clientWidth,
    min_widths = [],
        max_widths = [];

    // getComputedStyle( el, ":before" ).getPropertyValue( "content" ) // ???
    // let breakpoints = ( getComputedStyle( el, ':before' ).content )
    var breakpoints = getComputedStyle(el).content.split('"').join('') // Firefox returns quotes, e.g., '"Content string"'
    .trim().split(' ').forEach(function (breakpoint) {
      // console.log( breakpoint )
      breakpoint = parseInt(breakpoint, 10);
      if (width >= breakpoint) min_widths.push(breakpoint);else max_widths.push(breakpoint);
    });

    el.setAttribute('min-width', min_widths.join(' '));
    el.setAttribute('max-width', max_widths.join(' '));
  }

  // Setup
  selectEls();

  window.onresize = (0, _debounceJs2['default'])(doEls, 100);

  return {
    update: selectEls
  };
};

},{"./debounce.js":1}]},{},[2])
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9Eb21pbmljL1Byb2plY3RzLzYuIFBlcnNvbmFsL2VsZW1lbnQtcXVlcmllcy9ub2RlX21vZHVsZXMvZ3VscC1icm93c2VyaWZ5L25vZGVfbW9kdWxlcy9icm93c2VyaWZ5L25vZGVfbW9kdWxlcy9icm93c2VyLXBhY2svX3ByZWx1ZGUuanMiLCIvVXNlcnMvRG9taW5pYy9Qcm9qZWN0cy82LiBQZXJzb25hbC9lbGVtZW50LXF1ZXJpZXMvZGVib3VuY2UuanMiLCIvVXNlcnMvRG9taW5pYy9Qcm9qZWN0cy82LiBQZXJzb25hbC9lbGVtZW50LXF1ZXJpZXMvZmFrZV9iZmM1OGFkYS5qcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTs7O0FDQUEsSUFBSSxRQUFRLEdBQUcsU0FBWCxRQUFRLENBQVksSUFBSSxFQUFFLElBQUksRUFBRSxTQUFTLEVBQUU7QUFDN0MsTUFBSSxPQUFPLENBQUE7QUFDWCxTQUFPLFlBQVc7QUFDaEIsUUFBSSxPQUFPLEdBQUcsSUFBSTtRQUFFLElBQUksR0FBRyxTQUFTLENBQUE7QUFDcEMsZ0JBQVksQ0FBQyxPQUFPLENBQUMsQ0FBQTtBQUNyQixXQUFPLEdBQUcsVUFBVSxDQUFDLFlBQVc7QUFDOUIsYUFBTyxHQUFHLElBQUksQ0FBQTtBQUNkLFVBQUksQ0FBQyxTQUFTLEVBQUUsSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLEVBQUUsSUFBSSxDQUFDLENBQUE7S0FDMUMsRUFBRSxJQUFJLENBQUMsQ0FBQTtBQUNSLFFBQUksU0FBUyxJQUFJLENBQUMsT0FBTyxFQUFFLElBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxFQUFFLElBQUksQ0FBQyxDQUFBO0dBQ3JELENBQUE7Q0FDRixDQUFBOztBQUVELE1BQU0sQ0FBQyxPQUFPLEdBQUcsUUFBUSxDQUFBOzs7Ozs7Ozs7Ozs7Ozs7MEJDTEosZUFBZTs7OztBQUdwQyxNQUFNLENBQUMsT0FBTyxHQUFHLFlBQU07Ozs7OztBQU1yQixNQUFJLFFBQVEsR0FBRyxNQUFNLENBQUE7QUFDckIsTUFBSSxHQUFHLENBQUE7O0FBR1AsV0FBUyxTQUFTLEdBQUU7Ozs7Ozs7Ozs7Ozs7O0FBY2xCLE9BQUcsR0FBRyxLQUFLLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUE7O0FBRXJFLFNBQUssRUFBRSxDQUFBO0dBQ1I7O0FBR0QsV0FBUyxLQUFLLEdBQUU7QUFDZCxRQUFJLEdBQUcsSUFBSSxHQUFHLENBQUMsTUFBTSxFQUNuQixHQUFHLENBQUMsT0FBTyxDQUFFLHlCQUF5QixDQUFFLENBQUE7R0FDM0M7O0FBSUQsV0FBUyx5QkFBeUIsQ0FBQyxFQUFFLEVBQUM7QUFDcEMsV0FBTyxDQUFDLEdBQUcsQ0FBRSxFQUFFLENBQUMsV0FBVyxDQUFFLENBQUE7OztBQUc3QixRQUFJLEtBQUssR0FBRyxFQUFFLENBQUMsV0FBVzs7QUFDdEIsY0FBVSxHQUFHLEVBQUU7UUFDZixVQUFVLEdBQUcsRUFBRSxDQUFBOzs7O0FBS25CLFFBQUksV0FBVyxHQUFHLEFBQUUsZ0JBQWdCLENBQUMsRUFBRSxDQUFDLENBQUMsT0FBTyxDQUM3QyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQztLQUNuQixJQUFJLEVBQUUsQ0FDTixLQUFLLENBQUMsR0FBRyxDQUFDLENBQ1YsT0FBTyxDQUFDLFVBQUEsVUFBVSxFQUFJOztBQUVyQixnQkFBVSxHQUFHLFFBQVEsQ0FBQyxVQUFVLEVBQUUsRUFBRSxDQUFDLENBQUE7QUFDckMsVUFBSyxLQUFLLElBQUksVUFBVSxFQUN0QixVQUFVLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFBLEtBRTNCLFVBQVUsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUE7S0FDOUIsQ0FBQyxDQUFBOztBQUdKLE1BQUUsQ0FBQyxZQUFZLENBQUMsV0FBVyxFQUFFLFVBQVUsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQTtBQUNsRCxNQUFFLENBQUMsWUFBWSxDQUFDLFdBQVcsRUFBRSxVQUFVLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUE7R0FDbkQ7OztBQUlELFdBQVMsRUFBRSxDQUFBOztBQUVYLFFBQU0sQ0FBQyxRQUFRLEdBQUcsNkJBQVMsS0FBSyxFQUFFLEdBQUcsQ0FBQyxDQUFBOztBQUd0QyxTQUFPO0FBQ0wsVUFBTSxFQUFFLFNBQVM7R0FDbEIsQ0FBQTtDQUNGLENBQUEiLCJmaWxlIjoiZ2VuZXJhdGVkLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXNDb250ZW50IjpbIihmdW5jdGlvbiBlKHQsbixyKXtmdW5jdGlvbiBzKG8sdSl7aWYoIW5bb10pe2lmKCF0W29dKXt2YXIgYT10eXBlb2YgcmVxdWlyZT09XCJmdW5jdGlvblwiJiZyZXF1aXJlO2lmKCF1JiZhKXJldHVybiBhKG8sITApO2lmKGkpcmV0dXJuIGkobywhMCk7dGhyb3cgbmV3IEVycm9yKFwiQ2Fubm90IGZpbmQgbW9kdWxlICdcIitvK1wiJ1wiKX12YXIgZj1uW29dPXtleHBvcnRzOnt9fTt0W29dWzBdLmNhbGwoZi5leHBvcnRzLGZ1bmN0aW9uKGUpe3ZhciBuPXRbb11bMV1bZV07cmV0dXJuIHMobj9uOmUpfSxmLGYuZXhwb3J0cyxlLHQsbixyKX1yZXR1cm4gbltvXS5leHBvcnRzfXZhciBpPXR5cGVvZiByZXF1aXJlPT1cImZ1bmN0aW9uXCImJnJlcXVpcmU7Zm9yKHZhciBvPTA7bzxyLmxlbmd0aDtvKyspcyhyW29dKTtyZXR1cm4gc30pIiwidmFyIGRlYm91bmNlID0gZnVuY3Rpb24oZnVuYywgd2FpdCwgaW1tZWRpYXRlKSB7XG4gIHZhciB0aW1lb3V0XG4gIHJldHVybiBmdW5jdGlvbigpIHtcbiAgICB2YXIgY29udGV4dCA9IHRoaXMsIGFyZ3MgPSBhcmd1bWVudHNcbiAgICBjbGVhclRpbWVvdXQodGltZW91dClcbiAgICB0aW1lb3V0ID0gc2V0VGltZW91dChmdW5jdGlvbigpIHtcbiAgICAgIHRpbWVvdXQgPSBudWxsXG4gICAgICBpZiAoIWltbWVkaWF0ZSkgZnVuYy5hcHBseShjb250ZXh0LCBhcmdzKVxuICAgIH0sIHdhaXQpXG4gICAgaWYgKGltbWVkaWF0ZSAmJiAhdGltZW91dCkgZnVuYy5hcHBseShjb250ZXh0LCBhcmdzKVxuICB9XG59XG5cbm1vZHVsZS5leHBvcnRzID0gZGVib3VuY2VcbiIsIi8qXG4gIFRPRE9cbiAgLSBbIF0gSG93IHRvIGRlYWwgd2l0aCBjaXJjdWxhcml0eT8gRS5nLiwgcGFyZW50IGVsZW1lbnQgY2hhbmdpbmcgd2lkdGggd2l0aCBcbiAgICAgICAgbHRyaWdnZXJzIGEgbmV3IHdpZHRoIGNoYW5nZSBvbiB0aGUgY2hpbGQ/IFxuICAtIFsgXSBQbGF5aW5nIG5pY2Ugd2l0aCBtZWRpYSBxdWVyaWVzXG5cbiovXG5cbmltcG9ydCBkZWJvdW5jZSBmcm9tICcuL2RlYm91bmNlLmpzJ1xuXG5cbm1vZHVsZS5leHBvcnRzID0gKCkgPT4ge1xuICAvLyBUT0RPLCBncmFjZWZ1bCBmYWlsIG9yIHBvbHlmaWxsXG4gIC8vIHR5cGVvZiBkb2N1bWVudC5xdWVyeVNlbGVjdG9yQWxsID09IFwidW5kZWZpbmVkXCJcbiAgLy8gaWYgKCAhKCAncXVlcnlTZWxlY3RvcicgaW4gdy5kb2N1bWVudCApIHx8ICEoICdnZXRDb21wdXRlZFN0eWxlJyBpbiB3ICkgKVxuICAvLyAgIHJldHVyblxuXG4gIHZhciBzZWxlY3RvciA9ICdbZXFdJ1xuICB2YXIgZWxzXG4gIFxuICBcbiAgZnVuY3Rpb24gc2VsZWN0RWxzKCl7XG4gICAgLyogSW4gQ2hyb21lIG9uIE1hYyB0aGUgb3JkZXIgb2YgZWxlbWVudHMgaW4gcXVlcnlTZWxlY3RvckFsbCBpcyB0b3AtZG93bixcbiAgICBlLmcuLCBib2R5IGZpcnN0IHRoZW4gdGhlIGZpcnN0IGRpdiBpbiB0aGF0IHRoZW4gYWxsIHRoYXQgZmlyc3QgZGl2J3MgXG4gICAgZGVzY2VuZGFudHMsIHRoZW4gdGhlIHNlY29uZCBkaXYgaW4gdGhlIGJvZHkgZm9sbG93ZWQgYnkgYWxsIGl0cyBcbiAgICBkZXNjZW5kYW50cy4gXG4gICAgICBJZiB0aGlzIGlzIGNvbnNpc3RlbnQgaW4gYWxsIGJyb3dzZXJzIHRoZW4gY2lyY3VsYXJpdHkgaXMgcHJldmVudGVkIGJ5IFxuICAgIGZpcnN0IGFwcGx5aW5nIHBhcmVudCBlbGVtZW50IHN0eWxlcyAod2hpY2ggbWF5IG1lYW4gYSBqdW1wIHRvIDJ4IGNvbHVtbiwgXG4gICAgd2hpY2ggZWZmZWN0cyBjaGlsZHJlbiB3aWR0aHMgaW4gdHVybikgZm9sbG93ZWQgYnkgY2hpbGQgd2lkdGhzLiBcbiAgICAgIEJlY2F1c2Ugd2UncmUgbWVhc3VyaW5nIGVsZW1lbnQgd2lkdGhzLCBpdCBtYXkgYmUgbmVjZXNzYXJ5IHRvIG1hbnVhbGx5IFxuICAgIHRyaWdnZXIgcmUtcGFpbnRzIGZvciBjaGlsZHJlbiB0byBkZXRlY3QgY29ycmVjdCBzdHlsZXMuXG4gICAgICBGb3IgdGhpcyByZWFzb24sIGFkZGluZyBuZXcgZWxlbWVudHMgc2hvdWxkIHJlLXF1ZXJ5IHRoZSBET00gdG8gZ2V0IGEgbmV3IFxuICAgIHRyZWUgb3JkZXJlZCBlbGVtZW50IGxpc3QuXG4gICAgKi9cbiAgICBcbiAgICBlbHMgPSBBcnJheS5wcm90b3R5cGUuc2xpY2UuY2FsbChkb2N1bWVudC5xdWVyeVNlbGVjdG9yQWxsKHNlbGVjdG9yKSlcbiAgICAvLyBeIENvbnZlcnQgbm9kZSBsaXN0IHRvIGFycmF5XG4gICAgZG9FbHMoKVxuICB9XG4gIFxuICBcbiAgZnVuY3Rpb24gZG9FbHMoKXtcbiAgICBpZiAoZWxzICYmIGVscy5sZW5ndGgpXG4gICAgICBlbHMuZm9yRWFjaCggYXBwbHlCcmVha3BvaW50QXR0cmlidXRlcyApXG4gIH1cbiAgXG4gIFxuICBcbiAgZnVuY3Rpb24gYXBwbHlCcmVha3BvaW50QXR0cmlidXRlcyhlbCl7XG4gICAgY29uc29sZS5sb2coIGVsLmNsaWVudFdpZHRoIClcbiAgICAvLyByZXR1cm4gZmFsc2VcbiAgICBcbiAgICBsZXQgd2lkdGggPSBlbC5vZmZzZXRXaWR0aCwgLy8gY2xpZW50V2lkdGgsXG4gICAgICAgIG1pbl93aWR0aHMgPSBbXSxcbiAgICAgICAgbWF4X3dpZHRocyA9IFtdXG4gICAgXG4gICAgXG4gICAgLy8gZ2V0Q29tcHV0ZWRTdHlsZSggZWwsIFwiOmJlZm9yZVwiICkuZ2V0UHJvcGVydHlWYWx1ZSggXCJjb250ZW50XCIgKSAvLyA/Pz9cbiAgICAvLyBsZXQgYnJlYWtwb2ludHMgPSAoIGdldENvbXB1dGVkU3R5bGUoIGVsLCAnOmJlZm9yZScgKS5jb250ZW50IClcbiAgICBsZXQgYnJlYWtwb2ludHMgPSAoIGdldENvbXB1dGVkU3R5bGUoZWwpLmNvbnRlbnQgKVxuICAgICAgLnNwbGl0KCdcIicpLmpvaW4oJycpIC8vIEZpcmVmb3ggcmV0dXJucyBxdW90ZXMsIGUuZy4sICdcIkNvbnRlbnQgc3RyaW5nXCInXG4gICAgICAudHJpbSgpXG4gICAgICAuc3BsaXQoJyAnKVxuICAgICAgLmZvckVhY2goYnJlYWtwb2ludCA9PiB7XG4gICAgICAgIC8vIGNvbnNvbGUubG9nKCBicmVha3BvaW50IClcbiAgICAgICAgYnJlYWtwb2ludCA9IHBhcnNlSW50KGJyZWFrcG9pbnQsIDEwKVxuICAgICAgICBpZiAoIHdpZHRoID49IGJyZWFrcG9pbnQgKVxuICAgICAgICAgIG1pbl93aWR0aHMucHVzaChicmVha3BvaW50KVxuICAgICAgICBlbHNlIFxuICAgICAgICAgIG1heF93aWR0aHMucHVzaChicmVha3BvaW50KVxuICAgICAgfSlcbiAgICBcbiAgICBcbiAgICBlbC5zZXRBdHRyaWJ1dGUoJ21pbi13aWR0aCcsIG1pbl93aWR0aHMuam9pbignICcpKVxuICAgIGVsLnNldEF0dHJpYnV0ZSgnbWF4LXdpZHRoJywgbWF4X3dpZHRocy5qb2luKCcgJykpXG4gIH1cbiAgXG5cbiAgLy8gU2V0dXBcbiAgc2VsZWN0RWxzKClcbiAgXG4gIHdpbmRvdy5vbnJlc2l6ZSA9IGRlYm91bmNlKGRvRWxzLCAxMDApXG4gIFxuICBcbiAgcmV0dXJuIHtcbiAgICB1cGRhdGU6IHNlbGVjdEVscyxcbiAgfVxufVxuIl19
