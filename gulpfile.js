var gulp         = require('gulp'),
    less         = require('gulp-less'),
    browserify   = require('gulp-browserify')


var SRC_DIR = '.', // less, js etc that we want to compile/bundle
    DEST_DIR = './dest'
    
    
gulp.task("js", function() {
  gulp.src([
    SRC_DIR+'/eq.js'
  ])
  // .pipe(plumber(plumberErrorHandler))
  .pipe(browserify({
    debug: true
  }))
  .pipe(gulp.dest(DEST_DIR))
})


gulp.task('test',function() {
  gulp.src([
    './examples/app.js'
  ])
  // .pipe(plumber(plumberErrorHandler))
  .pipe(browserify({
    debug: true
  }))
  .pipe(gulp.dest('./examples/www'))
})


gulp.task('build', function() {
  // gulp.start('less')
  gulp.start('js')
  gulp.start('test')
})


gulp.task('watch', function() {
  // gulp.watch(SRC_DIR+'/less/**/*.less', ['less']);
  gulp.watch(SRC_DIR+'/eq.js', ['js', 'test']);
  gulp.watch(SRC_DIR+'/examples/app.js', ['test']);
})


// @TODO add clean task 

gulp.task('default', [], function() {
  gulp.start('build', 'watch')
})
